import 'package:api_rest_dio/src/shared/custom_dio/interceptors.dart';
import 'package:dio/dio.dart';

import '../constants.dart';

class CustomDio extends Dio {
  CustomDio() {
    options.baseUrl = BASE_URL;
    interceptors.add(CustomInterceptors());
  }
}
