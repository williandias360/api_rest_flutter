import 'package:dio/dio.dart';

class CustomInterceptors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) {
    print("Request[${options.method}] => PATH: ${options.path}");
    return options;
  }

  @override
  onResponse(Response response) {
    print("Request[${response.statusCode}] => PATH: ${response.request.path}");
  }

  @override
  onError(DioError e) {
    print("Request[${e.response.statusCode}] => PATH: ${e.request.path}");
    return e;
  }
}
