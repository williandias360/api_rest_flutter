import 'package:api_rest_dio/src/shared/custom_dio/custom_dio.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:api_rest_dio/src/app_widget.dart';
import 'package:api_rest_dio/src/app_bloc.dart';

class AppModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc((i) => AppBloc()),
      ];

  @override
  List<Dependency> get dependencies => [Dependency((i) => CustomDio())];

  @override
  Widget get view => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
